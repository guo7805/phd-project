function saveAvi(name, skelStruct, channels, frameLength)

% This is a function to save the motion as AVI film

clf

numFrames = size(channels, 1);

mov(1:numFrames) = struct('cdata', [], 'colormap', []);

handle = skelVisualise(channels(1, :), skelStruct);


% Get the limits of the motion.
xlim = get(gca, 'xlim');
minY1 = xlim(1);
maxY1 = xlim(2);
ylim = get(gca, 'ylim');
minY3 = ylim(1);
maxY3 = ylim(2);
zlim = get(gca, 'zlim');
minY2 = zlim(1);
maxY2 = zlim(2);
for i = 1:size(channels, 1)
  Y = skel2xyz(skelStruct, channels(i, :));
  minY1 = min([Y(:, 1); minY1]);
  minY2 = min([Y(:, 2); minY2]);
  minY3 = min([Y(:, 3); minY3]);
  maxY1 = max([Y(:, 1); maxY1]);
  maxY2 = max([Y(:, 2); maxY2]);
  maxY3 = max([Y(:, 3); maxY3]);
end
xlim = [minY1 maxY1];
ylim = [minY3 maxY3];
zlim = [minY2 maxY2];
set(gca, 'xlim', xlim, ...
         'ylim', ylim, ...
         'zlim', zlim);

[X, Y] = meshgrid(minY1:0.5:maxY1, minY2:0.5:maxY2);
Z = repmat([minY2], size(X, 1), size(X, 2));

% Play the motion
for j = 1:size(channels, 1)
	axis off
  	pause(frameLength)
	skelModify(handle, channels(j, :), skelStruct);
	hold on;
%	surf(X, Z, Y);
	mov(j) = getframe(gcf);
%	aviobj = addframe(aviobj, frame);
end

%avifinal = close(aviobj);
movie2avi(mov, name, 'compression', 'None', 'fps', 60);

%aviFinal = true;
