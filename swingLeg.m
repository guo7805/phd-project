function swingCh = swingLeg(standing, chswing_start, chswing_end, numFrames)

% the function returns the channels of swing leg

swingCh = zeros(numFrames, length(chswing_start));

if numFrames < 2000
	for i = 1 : length(chswing_start)
		if strcmp(standing, 'right') 
			switch i
				case 2
					swingCh(1:ceil(numFrames/2), i) = linspace(chswing_start(i), chswing_end(i)+10, ceil(numFrames/2));
					swingCh(ceil(numFrames/2)+1:end, i) = linspace(chswing_end(i)+10, chswing_end(i), numFrames - ceil(numFrames/2));
				case 3
					swingCh(1:ceil(numFrames/2), i) = linspace(chswing_start(i), chswing_end(i)+5, ceil(numFrames/2));
					swingCh(ceil(numFrames/2)+1:end, i) = linspace(chswing_end(i)+5, chswing_end(i), numFrames - ceil(numFrames/2));
				case 5
					swingCh(1:ceil(numFrames/2), i) = linspace(chswing_start(i), chswing_end(i)-10, ceil(numFrames/2));
					swingCh(ceil(numFrames/2)+1:end, i) = linspace(chswing_end(i)-10, chswing_end(i), numFrames - ceil(numFrames/2));
				case 6
					swingCh(1:ceil(numFrames/2), i) = linspace(chswing_start(i), chswing_end(i)-10, ceil(numFrames/2));
					swingCh(ceil(numFrames/2)+1:end, i) = linspace(chswing_end(i)-10, chswing_end(i), numFrames - ceil(numFrames/2));
				case 8
					swingCh(1:ceil(numFrames/2), i) = linspace(chswing_start(i), chswing_end(i)-10, ceil(numFrames/2));
					swingCh(ceil(numFrames/2)+1:end, i) = linspace(chswing_end(i)-10, chswing_end(i), numFrames - ceil(numFrames/2));
				case 9
					swingCh(1:ceil(numFrames/2), i) = linspace(chswing_start(i), chswing_end(i)+5, ceil(numFrames/2));
					swingCh(ceil(numFrames/2)+1:end, i) = linspace(chswing_end(i)+5, chswing_end(i), numFrames - ceil(numFrames/2));
				otherwise
					swingCh(:, i) = linspace(chswing_start(i), chswing_end(i), numFrames);
			end
			
		else
			switch i 
				case 2
					swingCh(1:ceil(numFrames/2), i) = linspace(chswing_start(i), chswing_end(i)+10, ceil(numFrames/2));
					swingCh(ceil(numFrames/2)+1:end, i) = linspace(chswing_end(i)+10, chswing_end(i), numFrames - ceil(numFrames/2));
				case 3
					swingCh(1:ceil(numFrames/2), i) = linspace(chswing_start(i), chswing_end(i)+10, ceil(numFrames/2));
					swingCh(ceil(numFrames/2)+1:end, i) = linspace(chswing_end(i)+10, chswing_end(i), numFrames - ceil(numFrames/2));
				case 5
					swingCh(1:ceil(numFrames/2), i) = linspace(chswing_start(i), chswing_end(i)+10, ceil(numFrames/2));
					swingCh(ceil(numFrames/2)+1:end, i) = linspace(chswing_end(i)+10, chswing_end(i), numFrames - ceil(numFrames/2));
				case 6
					swingCh(1:ceil(numFrames/2), i) = linspace(chswing_start(i), chswing_end(i)-5, ceil(numFrames/2));
					swingCh(ceil(numFrames/2)+1:end, i) = linspace(chswing_end(i)-5, chswing_end(i), numFrames - ceil(numFrames/2));
				case 8
					swingCh(1:ceil(numFrames/2), i) = linspace(chswing_start(i), chswing_end(i)-10, ceil(numFrames/2));
					swingCh(ceil(numFrames/2)+1:end, i) = linspace(chswing_end(i)-10, chswing_end(i), numFrames - ceil(numFrames/2));
				case 9
					swingCh(1:ceil(numFrames/2), i) = linspace(chswing_start(i), chswing_end(i)-5, ceil(numFrames/2));
					swingCh(ceil(numFrames/2)+1:end, i) = linspace(chswing_end(i)-5, chswing_end(i), numFrames - ceil(numFrames/2));
				otherwise
					swingCh(:, i) = linspace(chswing_start(i), chswing_end(i), numFrames);				
			end			
		end
	end
else
	tempnumFrames = 500;
	for i = 1 : length(chswing_start)
		swingCh(1:tempnumFrames, i) = linspace(chswing_start(i), chswing_end(i), tempnumFrames);
	end
	swingCh(501:end, :) = repmat(swingCh(500, :), numFrames - tempnumFrames, 1);
end



