NewChannels = channels;
startturning = logframes(16);
middlepoint = logframes(18);
endpoint = logframes(20);
startangle = NewChannels(startturning, 4);
endangle = NewChannels(endpoint, 4);

for i = startturning : middlepoint
	NewChannels(i, 4) = startangle*(1 - (i-startturning)/(middlepoint - startturning));

	% update root pos
	NewChannels(i, 1) = NewChannels(i, 1) + (NewChannels(i, 1) - NewChannels(i-1, 1))*cos(NewChannels(i, 4)/180*pi);
	NewChannels(i, 2) = NewChannels(i, 2) + (NewChannels(i, 1) - NewChannels(i-1, 1))*sin(NewChannels(i, 4)/180*pi);

	% IK to update the standing legs
	
end

for i = middlepoint : endpoint
	NewChannels(i, 4) = endangle*(i-middlepoint)/(endpoint - middlepoint);

	% update root pos
	NewChannels(i, 1) = NewChannels(i, 1) + (NewChannels(i, 1) - NewChannels(i-1, 1))*cos(NewChannels(i, 4)/180*pi);
	NewChannels(i, 2) = NewChannels(i, 2) + (NewChannels(i, 1) - NewChannels(i-1, 1))*sin(NewChannels(i, 4)/180*pi);

	% IK to update the standing legs

end
