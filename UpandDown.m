point1 = logframes(10);
point2 = logframes(30);
point3 = logframes(40);


channels_down = channels(1:point1, :);
channels_flat = channels(point1:point2, :);
channels_up = channels(point2:point3, :);
channels_flat2 = channels(point3:end, :);

for i = 1 : size(channels_down, 1)
	channels_down(i, 1) = channels_down(end, 1) + (channels_down(i, 1) - channels_down(end, 1))*cos(pi/18);
	channels_down(i, 3) = channels_down(end, 3) - (channels_down(i, 1) - channels_down(end, 1))*sin(pi/18);
end
channels_down(:, 6) = 10;

for i = 1 : size(channels_up, 1)
	channels_up(i, 1) = channels_up(1, 1) + (channels_up(i, 1) - channels_up(1, 1))*cos(pi/18); 
	channels_up(i, 3) = channels_up(1, 3) + (channels_up(i, 1) - channels_up(1, 1))*sin(pi/18);
end
channels_up(:, 6) = -10;

diffxyz = channels_up(end, 1:3) - channels(point3, 1:3);
channels_flat2(:, 1:3) = channels_flat2(:, 1:3) + repmat(diffxyz, size(channels_flat2, 1), 1);

Newchannels = [channels_down; channels_flat; channels_up; channels_flat2;];
