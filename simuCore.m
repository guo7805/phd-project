array_footxyz = [];
array_offxyz = [];
standing = 'right';
channels = [];
timestep = 0.001;
laststop = chin(1:3);



rightswingtarget = chin(chright);
leftswingtarget = - [chin(chlist(13:15)) chin(chlist(1:3)) chin(chlist(7:9))];


channels = [channels; chin];

options = odeset('Events', @switchevents);

for istep = 1 : 5

	logstanding = [logstanding standing];
	logframes = [logframes size(channels, 1)];

	if istep == 1
		% Initialization
		yin = [ chin(1: 3) 15 0 0 0 0];
%		nextstop = 0.0028;
		for ileg = 1 : numlegs/2
			array_footxyz = [array_footxyz ; endPos(skel, chin, ileg*8 - 3)];
			array_offxyz = [array_offxyz; skel.tree(ileg*8 - 6).offset];
		end
		lastangles = chin(chright);
	else
		% Update the initialization conditions for each stance
		yin = Y(end, :);
		laststop = yin(1:3);
%		nextstop = yin(1) + 0.0036;
		if strcmp(standing, 'right')
			standinglegs = [1 3 5];
			array_footxyz  = [];
			array_offxyz = [];
			for ileg = 1 : numlegs/2
				array_footxyz = [array_footxyz; endPos(skel, channels(end, :), ileg*8 - 3)];	% update the positions and offsets for left standing foot
				array_offxyz = [array_offxyz; skel.tree(ileg*8 - 6).offset];
			end
			lastangles = channels(end, chright);
		else
			standinglegs = [2 4 6];
			array_footxyz  = [];
			array_offxyz = [];
			for ileg = 1 : numlegs/2
				array_footxyz = [array_footxyz; endPos(skel, channels(end, :), ileg*8 + 1)];	% update the positions and offsets for left standing foot
				array_offxyz = [array_offxyz; skel.tree(ileg*8 - 2).offset];
			end
			lastangles = channels(end, chleft);
		end
	end

	controlparam = struct('amp', array_amp, 'off', array_off, 'omega', array_omega, 'pdcontrol', array_con);

	param = struct('controlparam', controlparam, 'l', l, 'm', m, 'numlegs', 6, 'timestep', timestep, 'standing', standing, 'footxyz', array_footxyz, 'offxyz', array_offxyz, 'laststop', laststop, 'lastangles', lastangles);
	[T, Y, TE, YE, IE] = ode45(@mainFun, [0: timestep : 10], yin, options, param);

	zrotation = [zrotation; Y(:, 4)];

	if ~isreal(Y)
		fitcost = NaN;
		return;
	end

	if sum(isnan(Y)) > 0
		fitcost = NaN;
		return;
	end


	for iframe = 1 : size(Y, 1)
		chtemp = zeros(size(chin));	
		rootpos = Y(iframe,1:3)';
		chtemp(1:3) = rootpos;

		for ileg = 1 : numlegs/2
			footxyz = array_footxyz(ileg, :)';
			offxyz = array_offxyz(ileg, :)';
		
			if strcmp(standing, 'right')
				[alpha, beta, gamma] = IKgeo(ileg*2-1, rootpos, footxyz, offxyz, 0); 		% set the zrot to 0
				chtemp(chright(ileg*3-2 : ileg*3)) = [alpha beta gamma];			%problem here
			else
				[alpha, beta, gamma] = IKgeo(ileg*2, rootpos, footxyz, offxyz, 0); 		% set the zrot to 0
				chtemp(chleft(ileg*3-2 : ileg*3)) = [alpha beta gamma];			%problem here
			end
		end

		channels = [channels; chtemp];

	end

	numFrames = size(Y, 1);

	if strcmp(standing, 'right')
		channels((end-size(Y,1) + 1):end, chleft) = swingLeg(channels(logframes(end), chleft), leftswingtarget, numFrames);
	else
		channels((end-size(Y,1) + 1):end, chright) = swingLeg(channels(logframes(end), chright), rightswingtarget, numFrames);
	end
	

	if strcmp(standing, 'right')
		standing = 'left';
	else
		standing = 'right';
	end

end

channels_array{i_opt} = channels;
