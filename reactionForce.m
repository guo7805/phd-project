function force = reactionForce(alpha, beta, gamma)

global l;

%Torque = [sin(3*t) sin(3*t) sin(3*t)]'
% replace this with a sophisticated CPG controller
Torque = [1 0 0]';



J = [ 	dxy*cos(alpha)			dxy*sin(alpha)			0;
	dz*sin(alpha)			-dz*cos(alpha)			-dxy;
	l*cos(beta + gamma)*sin(alpha) 	-l*cos(beta+gamma)*cos(alpha) -l*sin(beta+gamma)];


Fxyz = pinv(J) * Torque ;
