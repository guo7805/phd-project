function fitcost = optimalPhi(x)

	%angles_offsets = [-46.6386 0.4600 43.0844 46.6386 -0.4600 43.0844];
	%angles_amps = [13.3614 15.4600 13.0844 13.3614 15.4600 13.0844];
	omegas = [10 10 10 10 10 10];

	angles_offsets = x(1:6);
	angles_amps = x(7:12);

	numlegs = 6;
	l = [5 5; 5 5; 5 7.5; 5 5; 5 5; 5 7.5];
	m = 1;

	chlist = [];
	for ileg = 1 : numlegs
		chlist = [chlist 6+(ileg-1)*9+4 6+(ileg-1)*9+5 6+(ileg-1)*9+8];
	end


	chleft = [chlist(4:6) chlist(10:12) chlist(16:18)];
	chright = [chlist(1:3) chlist(7:9) chlist(13:15)];
	[skel,chin,f]=bvhReadFile('muscle.bvh');

	array_footxyz = zeros(3, 3);
	array_offxyz = zeros(3, 3);
	standing = 'right';
	timestep = 0.001;
	laststop = chin(1:3);
    	slopeA = 15;
	rightswingtarget = chin(chright);
	leftswingtarget = - [chin(chlist(13:15)) chin(chlist(1:3)) chin(chlist(7:9))];    

	options = odeset('Events', @switchevents);

	x2 = 0;
	y2 = 0;
	z2 = 0;
	xmax = 0;
	ytral = 0;
	ztral = 0;
    	fitcost = 0;
	chtemp = zeros(size(chin));	
	for istep = 1 : 20
		

		if istep == 1
		    % Initialization
		    yin = [ chin(1: 3) 15 0 0 0 0 0 0];
		    for ileg = 1 : numlegs/2
			array_footxyz(ileg, :) = endPos(skel, chin, ileg*8 - 3);
			array_offxyz(ileg, :) = skel.tree(ileg*8 - 6).offset;
		    end
		else
		    % Update the initialization conditions for each stance
		    yin = Y(end, :);
		    laststop = yin(1:3);
		    if strcmp(standing, 'right')
			for ileg = 1 : numlegs/2
			    array_footxyz(ileg, :) = endPos(skel, chtemp, ileg*8 - 3);	% update the positions and offsets for left standing foot
			    array_offxyz(ileg, :) = skel.tree(ileg*8 - 6).offset;
			end

		    else
			for ileg = 1 : numlegs/2
			    array_footxyz(ileg, :) =  endPos(skel, chtemp, ileg*8 + 1);	% update the positions and offsets for left standing foot
			    array_offxyz(ileg, :) = skel.tree(ileg*8 - 2).offset;
			end

		    end
		end

		controlparam = struct('angles_offsets', angles_offsets, 'angles_amps', angles_amps, 'omegas', omegas);
		param = struct('controlparam', controlparam, 'slopeA', slopeA, 'l', l, 'm', m, 'numlegs', 6, 'timestep', timestep, 'standing', standing, 'footxyz', array_footxyz, 'offxyz', array_offxyz, 'laststop', laststop);
		[T, Y, TE, YE, IE] = ode45(@mainFun, [0: timestep : 10], yin, options, param);


		if (~isreal(Y)) 
			%fitcost = NaN;
			return;
		elseif	 (sum(isnan(Y)) > 0) 
			%fitcost= NaN;
			return;
		elseif (abs(Y(end, 2)) > 1) || (abs(Y(end, 3)) > 1) 
			%fitcost = NaN;
			return;
		end
	       
		rootpos = Y(end,1:3)';
		chtemp(1:3) = rootpos;

		if strcmp(standing, 'right')
			standing = 'left';
			chtemp(chleft) = leftswingtarget;
		else
			standing = 'right';
			chtemp(chright) = rightswingtarget;
		end
		
		xyz = Y(:, 1:3);
		x2 = sum(diff(xyz(:, 1)).^2);
		y2 = sum(diff(xyz(:, 2)).^2);
		z2 = sum(diff(xyz(:, 3)).^2);
	
		xdist = xyz(end, 1) - laststop(1);
		

		ytral = ytral + sum(xyz(abs(xyz(:, 2))>0.2, 2).^2);
		ztral = ztral + sum(xyz(abs(xyz(:, 3))>0.2, 3).^2);
        	fitcost = fitcost +  200*(x2 + y2 + z2)/xdist + ytral + ztral - 100;
	end



clearvars -except fitcost
