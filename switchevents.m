function [value, isterminal, direction] = switchevents(t, y, param)

steplen = param.steplen;
numlegs = param.numlegs;
standing = param.standing;
array_footxyz = param.footxyz;
array_offxyz = param.offxyz;
laststop = param.laststop;
slopeAngle = param.slopeA/180*pi;

%groundheight = y(1)*sin(slopeAngle);

value = [];
isterminal = [];
direction = [];

value = [value; isreal(y); sum(isnan(y))-1];
isterminal = [isterminal; 1; 1];
direction = [direction; 0; 0];

for ileg = 1 : numlegs/2
	dist = norm(y(1:3) + array_offxyz(ileg, :)' - array_footxyz(ileg, :)');
	value = [value; dist - 10];
	isterminal = [isterminal; 1];
	direction = [direction; 0];
end

if strcmp(standing, 'right')
	m = [array_footxyz(1, 1:2) - array_footxyz(3, 1:2); y(1:2)' - array_footxyz(3, 1:2)];
else
	m = [array_footxyz(1, 1:2) - array_footxyz(2, 1:2); y(1:2)' - array_footxyz(2, 1:2)];
end



value = [value; det(m); y(1) - laststop(1) - steplen*cos(slopeAngle); norm(y(1:2) - laststop(1:2)')- steplen]; %; abs(y(2)) - 0.5; abs(y(3)) - laststop(3) - 0.5*cos(slopeAngle)];

isterminal = [isterminal; 1 ; 1; 1];% 1; 1];
direction = [direction; 0; 0; 0];% 0; 0];

