

global choice;

choice = 'opti';
% choice = 'simu';
% choice = 'optisimu';

if strcmp(choice, 'opti')
	clear;
	param = [0 0 0 -0.05 0 0; pi 0 0 0 pi/2 pi/2];
	x0 = [10 1 1 0.013 0.021 0.0125];

	f = @(x)optimalFun(x, param);

	options = optimset('LargeScale', 'off');

	options = optimset(options,'Display','iter');

	[x fval, exitflag, output] = fminunc(f, x0, options);
elseif strcmp(choice, 'simu')



else


end
