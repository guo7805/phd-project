function fitcost_array = optimalFun(x)

% given the amplitude of oscillators, get the phase differences between the oscillators
% the output is the energy consumption

% start the simulation and run until it stops, calculate its energy consumption
% which should be the proper optimization objective function ?

x

sizeofpop = size(x, 2);
fitcost_array = zeros(sizeofpop, 1);

parfor i_opt = 1 : size(x, 2)
	tempx = x(:, i_opt);
	array_con = [tempx(1:2)'; tempx(3:4)'; tempx(5:6)'];
	array_off = [-46.6386  112.3361 -101.8990 0.4600 109.6251  -84.7650 43.0844  111.7014  -98.3020 46.6386 -112.3361 101.8990 -0.4600 -109.6251  84.7650 -43.0844  -111.7014  98.3020] ;
	array_amp = [13.3614   2.5172 	17.2470	15.4600 10.1938  10.1130 13.0844 4.0162  17.9119 13.3614 2.5172 17.2470	15.4600 10.1938  10.1130 13.0844 4.0162  17.9119];
	array_omega = [20 20 20 20 40 40 20 20 20 20 20 20 20 40 40 20 20 20]/2;
	numlegs = 6;
	l = 5 ;
	m = 1;

	chlist = [];
	for ileg = 1 : numlegs
	chlist = [chlist 6+(ileg-1)*9+4 6+(ileg-1)*9+5 6+(ileg-1)*9+8];
	end


	chleft = [chlist(4:6) chlist(10:12) chlist(16:18)];
	chright = [chlist(1:3) chlist(7:9) chlist(13:15)];
	[skel,chin,f]=bvhReadFile('muscle.bvh');

	array_footxyz = zeros(3, 3);
	array_offxyz = zeros(3, 3);
	standing = 'right';
	timestep = 0.001;
	laststop = chin(1:3);
	rightswingtarget = chin(chright);
	leftswingtarget = - [chin(chlist(13:15)) chin(chlist(1:3)) chin(chlist(7:9))];    

	options = odeset('Events', @switchevents);

	x2 = 0;
	y2 = 0;
	z2 = 0;
	xmax = 0;
	ymax = 0;
	zmax = 0;
	wastex = 0;


	for istep = 1 : 5
		chtemp = zeros(size(chin));	

		if istep == 1
		    % Initialization
		    yin = [ chin(1: 3) 15 0 0 0 0];
		    for ileg = 1 : numlegs/2
			array_footxyz(ileg, :) = endPos(skel, chin, ileg*8 - 3);
			array_offxyz(ileg, :) = skel.tree(ileg*8 - 6).offset;
		    end
		else
		    % Update the initialization conditions for each stance
		    yin = Y(end, :);
			%yin(6) = -yin(6);
		    laststop = yin(1:3);
		    if strcmp(standing, 'right')
			for ileg = 1 : numlegs/2
			    array_footxyz(ileg, :) = endPos(skel, chtemp, ileg*8 - 3);	% update the positions and offsets for left standing foot
			    array_offxyz(ileg, :) = skel.tree(ileg*8 - 6).offset;
			end

		    else
			for ileg = 1 : numlegs/2
			    array_footxyz(ileg, :) =  endPos(skel, chtemp, ileg*8 + 1);	% update the positions and offsets for left standing foot
			    array_offxyz(ileg, :) = skel.tree(ileg*8 - 2).offset;
			end

		    end
		end

		controlparam = struct('amp', array_amp, 'off', array_off, 'omega', array_omega, 'pdcontrol', array_con);

		param = struct('controlparam', controlparam, 'l', l, 'm', m, 'numlegs', 6, 'timestep', timestep, 'standing', standing, 'footxyz', array_footxyz, 'offxyz', array_offxyz, 'laststop', laststop);
		[T, Y, TE, YE, IE] = ode45(@mainFun, [0: timestep : 10], yin, options, param);

		%zrotation = [zrotation; Y(:, 4)];

		if ~isreal(Y)
			fitcost_array(i_opt) = NaN;
			break;
		end

		if sum(isnan(Y)) > 0
			fitcost_array(i_opt) = NaN;
			break;
		end

		if abs(Y(end, 2)) > 1
			fitcost_array(i_opt) = NaN;
			break;
		end
	       
		rootpos = Y(end,1:3)';
		chtemp(1:3) = rootpos;

		if strcmp(standing, 'right')
			standing = 'left';
			chtemp(chleft) = leftswingtarget;
		else
			standing = 'right';
			chtemp(chright) = rightswingtarget;
		end
		
		xyz = Y(:, 1:3);
		x2 = x2 + sum(diff(xyz(:, 1)).^2);
		y2 = y2 + sum(diff(xyz(:, 2)).^2);
		z2 = z2 + sum(diff(xyz(:, 3)).^2);
		diffx = diff(xyz(:, 1));

		wastex = wastex + sum(diffx(diffx<0).^2);
		
		tempmax_x = max(xyz(1, :), 1);
		tempmax_yz = max(abs(xyz(:, 2:3)), 1);
		if  tempmax_x > xmax
		    xmax = tempmax_x(1);
		end
		if  tempmax_yz(1) > ymax
		    ymax = tempmax_yz(1);
		end
		if  tempmax_yz(2) > zmax
		    zmax = tempmax_yz(2);
		end
	end
	if fitcost_array(i_opt) == NaN
		fitcost_array(i_opt) == NaN;
	else
		fitcost_array(i_opt) = (x2+ y2 + z2)/xmax  + wastex + ymax + zmax;
	end

end

fitcost_array



