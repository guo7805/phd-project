function dy = mainFun(t, y, param)

% input:  xyz position of root for current frame
% output: xyz position of root for next frame


dy = zeros(size(y));

l = param.l;
m = param.m;
numlegs = param.numlegs;
timestep = param.timestep;
array_footxyz = param.footxyz;
array_offxyz = param.offxyz;
slopeA = param.slopeA/180*pi;
param.controlparam.istep = param.istep;

if strcmp(param.standing, 'right')
	standinglegs = [1 3 5];
else
	standinglegs = [2 4 6];
end

force = zeros(3, 3);
ztorque = zeros(3, 3);

parfor ileg = 1 : numlegs/2
%for ileg = 1 : numlegs/2
	footxyz = array_footxyz(ileg, :)';
	offxyz = array_offxyz(ileg, :)';
	l_leg = l(standinglegs(ileg), :);
	iforce = singleLeg(standinglegs(ileg), y, footxyz, offxyz, 0, timestep, param.controlparam, l_leg, -slopeA);
	force(ileg, :)  = iforce';
	ztorque(ileg, :) = cross([array_footxyz(ileg, 1:2) 0] - [y(1:2)' 0], iforce);
end

%y
%force

Fxyz = sum(force, 1);
Ztq = sum(ztorque(:, 3));



% calculate the xyz position of root, given the sum of reaction forces from all legs
dy(1:3) = y(6:8);
istep = param.istep;
if istep<10
	slopeA = -10;
elseif istep<30
	slopeA = 0;
elseif istep<40
	slopeA = 10;
else
	slopeA = 0;
end
slopeA = slopeA/180*pi;
dy(6) = Fxyz(1)/m - 9.8*sin(slopeA);
%dy(7) = Fxyz(2)/m;
%dy(8) = Fxyz(3)/m - 9.8*cos(slopeAngle);


%dy(4) = y(9);
%dy(9) = Ztq/m/l^2;

sigma = 1;
lambda = 10;
omega = 0.66666;

temp = -lambda*((y(5)^2+y(10)^2)-sigma);

dy(5) = temp*y(5)-omega*y(10);
dy(10) = omega*y(5)+temp*y(10);


%{
temp =  -((y(4)^2+y(8)^2)/15^2-1);
omega = 20;

dy(4) = temp*y(4) - omega*y(8);
dy(8) = omega*y(4) + temp*y(8);
%dy(8) = Ztq/m/l^2;
%}

%{
%uncomment this to make the ant stop at step = 4
if (param.istep == 10) 
	dy(1) = 0;
end
%}


%{
%uncomment this to add perturbation to the ant

push = true;
if (param.istep == 10)  && push && (t<0.3)
	dy(1) = -8;
%elseif (param.istep == 20) && push && (t<0.3)
%	dy(2) = -8;
end
%}



