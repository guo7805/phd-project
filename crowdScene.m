clear
numofagents = 10;

    chlist = [];
    for ileg = 1 : 6
        chlist = [chlist 6+(ileg-1)*9+4 6+(ileg-1)*9+5 6+(ileg-1)*9+8];
    end

for iagent = 1 : numofagents

	randlen = rand(3, 2);
	randlen = [randlen; randlen];
	
	[skel,chin,f]=bvhReadFile('ant.bvh');

	    array_footxyz = zeros(6, 3);
	    array_offxyz = zeros(6, 3);

	%change = [0.51 3.048; 0.539 3.632; 4.082 4.1650; 0.51 3.048; 0.539 3.632; 4.082 4.1650;];
	change = 3*(randlen - 0.5);
	
	for ijoint = 1 : 6
		array_footxyz(ijoint, :) = endPos(skel, chin, ijoint*4 + 1);
		array_footxyz(:, 3) = -2.8301;
		array_offxyz(ijoint, :) = skel.tree(ijoint*4 - 2).offset;
		skel.tree(4*ijoint).offset(3) = skel.tree(4*ijoint).offset(3) - change(ijoint, 1);
		skel.tree(4*ijoint+1).offset(3) = skel.tree(4*ijoint+1).offset(3) - change(ijoint, 2);
		%skel.tree(4*ijoint).offset(3) = skel.tree(4*ijoint).offset(3) - 5*randlen(ijoint, 1);
		%skel.tree(4*ijoint+1).offset(3) = skel.tree(4*ijoint+1).offset(3) - 5*randlen(ijoint, 2);
		%{
		if (ijoint == 1) || (ijoint == 4)
			skel.tree(4*ijoint).offset(3) = skel.tree(4*ijoint).offset(3) - 1.5*randlen(ijoint, 1);
			skel.tree(4*ijoint+1).offset(3) = skel.tree(4*ijoint+1).offset(3) - 1.5*randlen(ijoint, 2);
		elseif (ijoint == 3) || (ijoint == 6)
			skel.tree(4*ijoint).offset(3) = skel.tree(4*ijoint).offset(3) - 5*randlen(ijoint, 1);
			skel.tree(4*ijoint+1).offset(3) = skel.tree(4*ijoint+1).offset(3) - 5*randlen(ijoint, 2);
		end
		%}
		
		IKparam = struct('zrot', 0, 'yrot', 0, 'l', abs([skel.tree(4*ijoint).offset(3) skel.tree(4*ijoint+1).offset(3)]));
		[alpha, beta, gamma] = IKgeo(ijoint, chin(1:3)', array_footxyz(ijoint, :)', array_offxyz(ijoint, :)', IKparam);
		chin(chlist(ijoint*3-2:ijoint*3)) = [alpha beta gamma];
	end

	filename = sprintf('crowd/crowdAgent%d.bvh', iagent);
	bvhWriteFile(filename, skel, chin, f);
end
	
