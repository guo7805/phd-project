function [alpha, beta, gamma, J, invJ] = IKgeo(index, rootxyz, footxyz, offxyz, param)

% geometrical IK solution, the return value is in degree
% index = 1:6

yrot = param.yrot;
zrot = param.zrot;
l = param.l;


numlegs = 6;
l1 = l(1);
l2 = l(2);

zrot = zrot/180*pi;
zrotMatrix = [ cos(zrot) -sin(zrot) 0;	  sin(zrot) cos(zrot) 0;   0		0	1];	

yrot = yrot/180*pi;
yrotMatrix = [cos(yrot) 0 sin(yrot); 0 1 0; -sin(yrot) 0 cos(yrot)];
%thisRot = rotationMatrix(0, yrot, zrot, 'zxy');

%diffxyz = footxyz - rootxyz - yrotMatrix*zrotMatrix * offxyz ;
%diffxyz = zrotMatrix' * yrotMatrix' * diffxyz;
diffxyz = zrotMatrix'*yrotMatrix'*(footxyz - rootxyz);
diffxyz = diffxyz - offxyz;

if index > numlegs/2	% right foot

	alpha = atan(diffxyz(1)/(-diffxyz(2)));

	dxy = sqrt(diffxyz(1)^2+diffxyz(2)^2);
	dxyz = sqrt(diffxyz(1)^2+diffxyz(2)^2+diffxyz(3)^2);
	gamma = (pi - acos( (l1^2 + l2^2 - dxyz^2) /(2*l1*l2) ));

	dz = abs(diffxyz(3));
	tempangle = acos( (dxyz^2 + l1^2 - l2^2)/(2*l1*dxyz));
	beta = - ( atan(dxy/dz) + tempangle );	

%	J = [   -dxy*cos(alpha)			-dxy*sin(alpha)			0;
%		dz*sin(alpha)			-dz*cos(alpha)			dxy;
%		l*cos(beta + gamma)*sin(alpha)	-l*cos(beta+gamma)*cos(alpha)	-l*sin(beta+gamma)];

else 			% left foot

	alpha = - atan(diffxyz(1)/diffxyz(2));

	dxy = sqrt(diffxyz(1)^2+diffxyz(2)^2);
	dxyz = sqrt(diffxyz(1)^2+diffxyz(2)^2+diffxyz(3)^2);
	gamma = - (pi - acos( (l1^2 + l2^2 - dxyz^2) / (2*l1*l2 ) ));

	dz = abs(diffxyz(3));
	tempangle = acos( (dxyz^2 + l1^2 - l2^2)/(2*l1*dxyz));
	beta = atan(dxy/dz) + tempangle;	

%	J = [ 	dxy*cos(alpha)			dxy*sin(alpha)			0;
%		dz*sin(alpha)			-dz*cos(alpha)			-dxy;
%		l*cos(beta + gamma)*sin(alpha) 	-l*cos(beta+gamma)*cos(alpha) -l*sin(beta+gamma)];
end

Px = offxyz(1);
Py = offxyz(2);


J = [ 	-Py     0                           -l1*cos(beta)*sin(alpha);
        Px      0                           l1*cos(beta)*cos(alpha);
        0       Py*cos(alpha)-Px*sin(alpha)	l1*sin(beta)+(Py*cos(alpha)-Px*sin(alpha))];	

temp = Py*cos(alpha) - Px*sin(alpha);

invJ = [-cos(alpha)/temp	-sin(alpha)/temp			0;
	-Px*(l1*sin(beta)+temp)/l1/cos(beta)/temp^2	-Py*(l1*sin(beta)+temp)/l1/cos(beta)/temp^2		1/temp;
	Px/l1/cos(beta)/temp	Py/l1/cos(beta)/temp			0];

alpha = alpha/pi*180;
beta = beta/pi*180;
gamma = gamma/pi*180;


