function torque = controller(index, angles, anglesvel, timestep, controlparam)

% the cpg controller
% input: channels variables (alpha, beta, gamma)
%	 index of leg: 1, 2 ... 6
% output: torque for each channel

torque = zeros(3, 1);

l = 5;

alpha = angles(1);
alphavel = anglesvel(1);	% degree

    switch index
        case 1
            Px = 1;
            Py = 1;
        case 2
            Px = 0;
            Py = 1;
        case 3
            Px = -1;
            Py = 1;
        case 4
            Px = 1;
            Py = -1;
        case 5
            Px = 0;
            Py = -1;
        case 6
            Px = -1;
            Py = -1;
    end
%{	
    torque(1) = -(Py*cos(alpha) - Px*sin(alpha));
    torque(2) = 9.8*(Py*cos(alpha) - Px*sin(alpha))/3;
    torque(3) = 9.8*(l*sin(beta) + Py*cos(alpha) - Px*sin(alpha))/3;
%}

	%angles_amp = controlparam.angles_amp;
	%angles_off= controlparam.angles_offset;
	%amp = controlparam.amp;
	%temp = amp*inosci(index, [alpha alphavel], timestep, controlparam)/angles_amp*sqrt(Px^2+Py^2) + cos(angles_off);

	alpha_next = inosci(index, [alpha alphavel], timestep, controlparam);

	alpha_next = alpha_next/180*pi;
	beta = angles(2)/180*pi;
	temp = Py*cos(alpha_next) - Px*sin(alpha_next);

	torque(1) = - temp;
	torque(2) = 9.8*temp/3;
	torque(3) = 9.8*(l*sin(beta) + temp)/3;
end


function ynext = inosci(index, yin, timestep, controlparam)
	sigma = 1;
	lambda = 10;
	omega = controlparam.omega;
	amp = controlparam.angles_amp;
	off= controlparam.angles_offset;

	yin(1) = yin(1) - off;

	temp = -lambda*((yin(1)^2+yin(2)^2)/amp^2-sigma);

	dy = temp*yin(1)-omega*yin(2);
	ddy = omega*yin(1)+temp*yin(2);

	ynext = yin(1) + dy * timestep + off;
end

function torque = pdservo(index, ichan, yio, pdcontrol)

    yin = yio(1, :);
    yout = yio(2, :);
    
	array_con = pdcontrol((index+1)/2, :);

 %   if (index == 1) && (ichan == 2)
  %      yout
   %     yin
    %end
	Kp = array_con(ichan*2 - 1);
	Kd = array_con(ichan*2);
    

	torque = Kp*(yout(1) - yin(1)) + Kd*(yout(2) - yin(2));

end


