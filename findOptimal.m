% set the parameters and unknown

%array_amp = [10 1 1 0.013 0.021 0.0125];	% why the reverse simulation doesn't work as expected ?
%array_off = [0 0 0 -0.05 0 0];
%array_phase = [pi 0 0 0 pi/2 pi/2];		% the previous three are for the left legs, the latter three are for the right legs

clear;

%param = [10 20 20 10 20 20 10 20 20];
%x0 = [0 63 -40 0 63 -40 0 -63 40 30 3 10 30 3 10 30 3 10];
%x0 = [-45 63 -50 0 63 -30 45 60 -40 15  5  20  15 3 5 15 15 30 ];
%x0 = [-45 112 -100 0 109 -85 45 112 -100 15 3 16 15 1 1 15 4 15 5 5 5 5 5 5];

x0 = [-45 112 -100 0 109 -85 45 112 -100 15 3 16 15 1 1 15 4 15];

%opts.UBounds = [-30 130 -120 30 130 -60 60 130 -75 30  10  30  30 10 10 30 20 30 10 10 10 10 10 10]; 
%opts.LBounds = [-60 80 -80 -30 80 -120 30 90 -125 0 0 0 0 0 0 0 0 0 0 0]; 
opts.UBounds = [-30 130 -80 30 130 -60 60 130 -75 30  10  30  30 10 10 30 20 30]';
opts.LBounds = [-60 80 -120 -30 80 -120 30 90 -125 0 0 0 0 0 0 0 0 0 ]';
opts.EvalParallel = 'yes'



%[xmin, fmin, counteval, stopflag] = cmaes('optimalFun', x0, [2 2 2 2 2 2 2 2 2 1 1 1 1 1 1 1 1 1]', opts);
[xmin, fmin, counteval, stopflag] = cmaes('optimalFun', x0, [], opts);



%{
f = @(x)optimalFun(x, param);

options = optimset('LargeScale', 'off');

options = optimset(options,'Display','iter');

options = optimset(options, 'MaxFunEvals', 100);

%[x fval, exitflag, output] = fmincon(f, x0, [],[],[],[],[],[],@confun, options);
[x fval, exitflag, output] = fminunc(f, x0, options);
%}

