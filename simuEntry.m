%clearvars -except x0 x xmin;

%diary('outputlog.txt');


array_amp = [];
array_off = [];
array_omega = [];
array_con = [];

numlegs = 6;
l = 5 ;
m = 1;

chlist = [];
for ileg = 1 : numlegs
	chlist = [chlist 6+(ileg-1)*9+4 6+(ileg-1)*9+5 6+(ileg-1)*9+8];
end


chleft = [chlist(4:6) chlist(10:12) chlist(16:18)];
chright = [chlist(1:3) chlist(7:9) chlist(13:15)];

%array_omega = [10 20 20 10 20 20 10 20 20 10 20 20 10 20 20 10 20 20];
array_omega = [20 20 20 20 40 40 20 20 20 20 20 20 20 40 40 20 20 20]/2;
[skel,chin,f]=bvhReadFile('muscle.bvh');



% reset the chin
%chin(chright) = array_off([1 2 3 7 8 9 13 14 15]) + array_amp([1 2 3 7 8 9 13 14 15]);
%chin(chleft) = array_off([4 5 6 10 11 12 16 17 18]) + array_amp([4 5 6 10 11 12 16 17 18]);

if findPD == true
	array_off = [-46.6386  112.3361 -101.8990 0.4600 109.6251  -84.7650 43.0844  111.7014  -98.3020 46.6386 -112.3361 101.8990 -0.4600 -109.6251  84.7650 -43.0844  -111.7014  98.3020] ;
	array_amp = [13.3614   2.5172 	17.2470	15.4600 10.1938  10.1130 13.0844 4.0162  17.9119 13.3614 2.5172 17.2470	15.4600 10.1938  10.1130 13.0844 4.0162  17.9119];
	array_con = [x_pd(1:2)'; x_pd(3:4)'; x_pd(5:6)'];
else
	if exist('xmin', 'var')
		array_off = [xmin(1:9); -xmin(1:9)];		% This is wrong?
		array_amp = [xmin(10:18); xmin(10:18)];

	elseif exist('x', 'var')
		array_off = [x(1:9); -x(1:9)];
		array_amp = [x(10:18); x(10:18)];
	else

	%	array_off = [-45 63 -50 0 0 0 45 60 -40 0 0 0 0 -63 30 0 0 0 ];
	%	array_off = [-45 63 -50 0 63 -30 45 60 -40 45 -63 50 0 -63 30  -45 -60 40];		% normal gait
	%	array_off = [-45 63 -50   0 63 -30 45 60 -40 45 -63 50 0 -63 30  -45 -60 40];		% turn right
		array_off = [-46.6386  112.3361 -101.8990 0.4600 109.6251  -84.7650 43.0844  111.7014  -98.3020 46.6386 -112.3361 101.8990 -0.4600 -109.6251  84.7650 -43.0844  -111.7014  98.3020] ;
		array_amp = [13.3614   2.5172 	17.2470	15.4600 10.1938  10.1130 13.0844 4.0162  17.9119 13.3614 2.5172 17.2470	15.4600 10.1938  10.1130 13.0844 4.0162  17.9119];
	end
	array_con = [100 100; 20 20; 50 50];
end
%array_off
%array_amp

simuCore;
