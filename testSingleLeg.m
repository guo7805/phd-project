% The script to test the function SingleLeg

clear;

global array_footxyz array_offxyz l skel; 

numlegs = 3;
chlist = [];
for ileg = 1 : numlegs
	chlist = [chlist 6+(ileg-1)*9+4 6+(ileg-1)*9+5 6+(ileg-1)*9+8];
end


x = 0: 0.01 : 1.5;
y = zeros(size(x));
z = zeros(size(x));

[skel,chin,f]=bvhReadFile('guo/Neuro/pointmass/muscle.bvh');

array_footxyz = [endPos(skel, chin, 5); endPos(skel, chin, 9); endPos(skel, chin, 13);];	
array_offxyz = [skel.tree(3).offset; skel.tree(7).offset; skel.tree(11).offset];

channels = [];

for i = 1 : size(x, 2)
	chtemp = zeros(size(chin));
	rootxyz = [x(i) y(i) z(i)]';
	chtemp(1:3) = rootxyz;
	for j = 1 : 3
		[a, b, c, J] = IKgeo(j, rootxyz, 0);
		chtemp(chlist(j*3-2 : j*3)) = [a b c];
	end
	channels = [channels; chtemp];
end
