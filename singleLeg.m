function force = singleLeg(index, y, footxyz, offxyz, zrot, timestep, controlparam, l, yawAngle)

% index = 1:6, is the index in the leg array

rootxyz = y(1:3);
rootvel = y(6:8);

IKparam = struct('zrot', zrot, 'yrot', yawAngle, 'l', l);

[alpha, beta, gamma, J, invJ] = IKgeo(index, rootxyz, footxyz, offxyz, IKparam);

anglesvel = J\rootvel;

anglesvel = anglesvel/pi*180;

%anglesvel = [0 0 0 ]';
legparam = struct('angles_offset', controlparam.angles_offsets(index),'angles_amp', controlparam.angles_amps(index), 'omega', controlparam.omegas(index), 'istep', controlparam.istep);

torque = controller( index, [alpha beta gamma], anglesvel, timestep, legparam);


force = invJ'*torque;
%force = inv(J)'*torque;
