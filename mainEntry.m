    
%array_off = [-46.6386  112.3361 -101.8990 0.4600 109.6251  -84.7650 43.0844  111.7014  -98.3020 46.6386 -112.3361 101.8990 -0.4600 -109.6251  84.7650 -43.0844  -111.7014  98.3020] ;
%    array_amp = [13.3614   2.5172 	17.2470	15.4600 10.1938  10.1130 13.0844 4.0162  17.9119 13.3614 2.5172 17.2470	15.4600 10.1938  10.1130 13.0844 4.0162  17.9119];
 %   array_omega = [20 20 20 20 20 20 20 20 20 20 20 20 20 20 20 20 20 20];
	%angles_offsets = [-46.6386 0.4600 43.0844 46.6386 -0.4600 -43.0844]; % problem here, ooops, the last number should be -43.0844
	%angles_amps = [13.3614 15.4600 13.0844 13.3614 15.4600 13.0844];

%-50	1.00000100000000	50.0000100000000	9.99999700000000	14.9290100000000	17.9929900000000

	% reset with optimized parameters

	clear
	
	optimalParam = [-33.9274400000000	14.6840200000000	58.3286300000000	33.3597700000000	-6.30873400000000	-47.0463400000000	5.14433600000000	11.8459400000000	4.82797300000000	19.3329000000000	5.59632300000000	20.1836800000000];

	%angles_offsets = [-38.1793300000000	1.70509900000000	45.4756300000000	57.0972800000000	13.0164400000000	-48.5356600000000];
	%angles_amps = [20.0085900000000	14.2143900000000	20.0167500000000	15.4855000000000	17.2085900000000	19.9697800000000];
    angles_offsets = optimalParam(1:6);
    angles_amps = optimalParam(7:12);


	omegas = [10 10 10 10 10 10];

    numlegs = 6;

%    l = [5 5; 5 5; 5 7.5; 5 5; 5 5; 5 7.5];
    m = 1;
    slopeA = 0;





    [skel,chin,f]=bvhReadFile('ant.bvh');
	chin(4) = 0;
    chlist = [];
	l = [];
    for ileg = 1 : numlegs
        chlist = [chlist 6+(ileg-1)*9+4 6+(ileg-1)*9+5 6+(ileg-1)*9+8];
	l = [l; -skel.tree(ileg*4).offset(3) -skel.tree(ileg*4+1).offset(3)];
    end
    chleft = [chlist(4:6) chlist(10:12) chlist(16:18)];
    chright = [chlist(1:3) chlist(7:9) chlist(13:15)];

	chin(6) = -slopeA;

	rightmiddlefoot = endPos(skel, chin, 21);
	steplen = 2*rightmiddlefoot(1);

    array_footxyz = zeros(3, 3);
    array_offxyz = zeros(3, 3);
    standing = 'right';
    timestep = 0.001;
    laststop = chin(1:3);
    rightswingtarget = chin(chright);
    leftswingtarget = - [chin(chlist(13:15)) chin(chlist(1:3)) chin(chlist(7:9))];    

    options = odeset('Events', @switchevents);
    
	logchtemp = [];
    chtemp = zeros(size(chin));	
	channels = chin;
	logframes = [];

	xmax = 0;
	ymax = 0;
	zmax = 0;
	x2 = 0;
	y2 = 0;
	z2 = 0;
	ytral = 0;
	ztral = 0;

    for istep = 1 : 50
           

	logframes = [logframes size(channels, 1)];
	
        if istep == 1
            % Initialization
            yin = [ chin(1: 3) 15 1 0 0 0 0 0];
            for ileg = 1 : numlegs/2
                array_footxyz(ileg, :) = endPos(skel, chin, ileg*8 - 3);
                array_offxyz(ileg, :) = skel.tree(ileg*8 - 6).offset;
            end
        else
            % Update the initialization conditions for each stance
            yin = Y(end, :);
            %yin(7) = -yin(7);
            laststop = yin(1:3);
            if strcmp(standing, 'right')
                for ileg = 1 : numlegs/2
                    array_footxyz(ileg, :) = endPos(skel, chtemp, ileg*8 - 3);	% update the positions and offsets for left standing foot
                    array_offxyz(ileg, :) = skel.tree(ileg*8 - 6).offset;
                end

            else
                for ileg = 1 : numlegs/2
                    array_footxyz(ileg, :) = endPos(skel, chtemp, ileg*8 + 1);	% update the positions and offsets for left standing foot
                    array_offxyz(ileg, :) = skel.tree(ileg*8 - 2).offset;
                end

            end
        end

        %controlparam = struct('amp', array_amp, 'off', array_off, 'omega', array_omega);
        controlparam = struct('angles_offsets', angles_offsets, 'angles_amps', angles_amps, 'omegas', omegas);
        param = struct('controlparam', controlparam, 'slopeA', slopeA, 'l', l, 'm', m, 'numlegs', 6, 'timestep', timestep, 'standing', standing, 'footxyz', array_footxyz, 'offxyz', array_offxyz, 'laststop', laststop, 'istep', istep, 'steplen', steplen);
        [T, Y, TE, YE, IE] = ode45(@mainFun, [0: timestep : 2], yin, options, param);

        %zrotation = [zrotation; Y(:, 4)];

        if ~isreal(Y)
    		break;
        end

        if sum(isnan(Y)) > 0
    		break;
        end
        
	if strcmp(standing, 'right')
		standinglegs = [1 3 5];
	else
		standinglegs = [2 4 6];
	end

	for iframe = 1 : size(Y, 1)
			chtemp = zeros(size(chin));	
			

			%chtemp(4:5) = [Y(iframe, 5) Y(iframe, 10)];
			%

			chtemp(4) = Y(iframe, 5)*10;
			if istep == 10
				chtemp(4) = channels(end, 4);
			end

%{
			if (istep >= 5) && (istep <=7) 
				chtemp(4) = channels(end, 4) + 0.01;
			elseif (istep > 7) && (istep < 9)
				chtemp(4) = channels(end, 4) - 0.01;
			end
			
			rootpos = Y(iframe,1:3)';
			if iframe > 1
				rootpos(1) = channels(end, 1) + (Y(iframe, 1) - Y(iframe-1, 1))*cos(chtemp(4)/180*pi);
				rootpos(2) = channels(end, 2) + (Y(iframe, 1) - Y(iframe-1, 1))*sin(chtemp(4)/180*pi);
			end

			chtemp(1:3) = rootpos;
%}

			%chtemp(4) = 10;
			rootpos = Y(iframe,1:3)';
			chtemp(1:3) = rootpos;
			chtemp(6) = -slopeA;
            		chtemp(3) = chtemp(1)*sin(slopeA/180*pi);
			for ileg = 1 : numlegs/2
				footxyz = array_footxyz(ileg, :)';
				offxyz = array_offxyz(ileg, :)';
				IKparam = struct('zrot', chtemp(4), 'yrot', -slopeA, 'l', l(standinglegs(ileg), :));
				if strcmp(standing, 'right')
					
					[alpha, beta, gamma] = IKgeo(ileg*2-1, rootpos, footxyz, offxyz, IKparam); 		% set the zrot to 0
					chtemp(chright(ileg*3-2 : ileg*3)) = [alpha beta gamma];			%problem here
				else
					[alpha, beta, gamma] = IKgeo(ileg*2, rootpos, footxyz, offxyz, IKparam); 		% set the zrot to 0
					chtemp(chleft(ileg*3-2 : ileg*3)) = [alpha beta gamma];			%problem here
				end
			end

			channels = [channels; chtemp];

		end

	Y(end, 5) = channels(end, 4)/10;
	numFrames = size(Y, 1);

	if strcmp(standing, 'right')        
		channels((end-size(Y,1) + 1):end, chleft) = swingLeg(standing, channels(logframes(end), chleft), leftswingtarget, numFrames);
        standing = 'left';
        chtemp(chleft) = leftswingtarget;
    else     
		channels((end-size(Y,1) + 1):end, chright) = swingLeg(standing, channels(logframes(end), chright), rightswingtarget, numFrames);
        standing = 'right';
        chtemp(chright) = rightswingtarget;
	end

        Y(end, :)
        %{
	rootpos = Y(end,1:3)';
        chtemp(1:3) = rootpos;		

        if strcmp(standing, 'right')
            standing = 'left';
            
        else
            standing = 'right';
            
        end
        Y(end, :)
        logchtemp = [logchtemp; chtemp];
	%}

	xyz = Y(:, 1:3);
	x2 = x2 + sum(diff(xyz(:, 1)).^2)
	y2 = y2 + sum(diff(xyz(:, 2)).^2)
	z2 = z2 + sum(diff(xyz(:, 3)).^2)

	tempmax_x = max(xyz(:, 1));
	tempmax_y = max(abs(xyz(:, 2)));
	tempmax_z = max(abs(xyz(:, 3)));
	if  tempmax_x > xmax
	    xmax = tempmax_x;
	end
	if  tempmax_y> ymax
	    ymax = tempmax_y;
	end
	if  tempmax_z > zmax
	    zmax = tempmax_z;
	end

	ytral = ytral + sum(xyz(abs(xyz(:, 2))>0.25, 2).^2)
	ztral = ztral + sum(xyz(abs(xyz(:, 3))>0.25, 3).^2)

    end
%{
x2
y2
z2
xmax
ymax
zmax
ytral
ztral
%}
