x0 = [100 100 20 20 50 50];

opts.UBounds = [200 200 100 100 150 150]';
opts.LBounds = [0 0 0 0 0 0]';
opts.EvalParallel = 'yes'


matlabpool

[xmin, fmin, counteval, stopflag] = cmaes('optimalFun', x0, [], opts);

clear

matlabpool close

