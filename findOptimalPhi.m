%x0 = [0 0 0 0 0 0 0 0 0 0 0 0];

%opts.UBounds = [1 1 1 1 1 1 pi pi pi pi pi pi]';
%opts.LBounds = [-1 -1 -1 -1 -1 -1 4 -pi -pi -pi -pi -pi -pi]';
%opts.EvalParallel = 'yes';

x0 = [-46.6386 0.4600 43.0844 46.6386 -0.4600 -43.0844 13.3614 15.4600 13.0844 13.3614 15.4600 13.0844];
opts.UBounds = [-30 15 60 60 15 -30 25 25 25 25 25 25]';
opts.LBounds = [-60 -15 30 30 -15 -60 5 5 5 5 5 5]';
OPTS.PopSize = 200;
%opts.Restarts = 3;
%opts.IncPopSize = 2;
matlabpool

[xmin, fmin, counteval, stopflag] = cmaes('optimalPhi', x0, [], opts);

matlabpool close
