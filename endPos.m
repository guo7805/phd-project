function endPos = endPos(skel, channels, index)

% given the skeletal and channels, calculate the position of specific endeffctors (FK)

% find the joint chain
chain = [];
chain = [index chain];	

while skel.tree(index).parent ~= 0
	index = skel.tree(index).parent;
	chain = [index chain];	
end


for i = 1:length(chain)  
  if ~isempty(skel.tree(chain(i)).posInd)
    xpos = channels(skel.tree(chain(i)).posInd(1));
    ypos = channels(skel.tree(chain(i)).posInd(2));
    zpos = channels(skel.tree(chain(i)).posInd(3));
  else
    xpos = 0;
    ypos = 0;
    zpos = 0;
  end
  rotStruct(i) = struct('rotation', []); 
  if nargin < 2 | isempty(skel.tree(chain(i)).rotInd)
    xangle = 0;
    yangle = 0;
    zangle = 0;
  else
    xangle = deg2rad(channels(skel.tree(chain(i)).rotInd(1)));
    yangle = deg2rad(channels(skel.tree(chain(i)).rotInd(2)));
    zangle = deg2rad(channels(skel.tree(chain(i)).rotInd(3)));
  end
  thisRotation = rotationMatrix(xangle, yangle, zangle, skel.tree(chain(i)).order);
%  thisPosition = [xpos ypos zpos];
  if ~skel.tree(chain(i)).parent
    rotStruct(i).rotation = thisRotation;
%    xyzStruct(i).xyz = skel.tree(chain(i)).offset + thisPosition;
  else
%    xyzStruct(i).xyz = ...
%        (skel.tree(chain(i)).offset + thisPosition)*xyzStruct(skel.tree(chain(i)).parent).rotation ...
%        + xyzStruct(skel.tree(chain(i)).parent).xyz;
    rotStruct(i).rotation = thisRotation*rotStruct(i-1).rotation;
    
  end
end

endPos = channels(1, 1:3);

for i = 2:length(chain)
	off = skel.tree(chain(i)).offset;
	endPos = endPos + off*rotStruct(i-1).rotation;
end
