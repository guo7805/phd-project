footNode = [9 17 25];

arrayTraje = zeros(size(channels, 1), 9);

for ileg = 1 : 3
	for iframe = 1 : size(channels, 1)
		arrayTraje(iframe, ileg*3-2:ileg*3) = endPos(skel, channels(iframe, :), footNode(ileg));
	end
end
